package ukrtech.formregister.autotest.BaseTest;
import com.codeborne.selenide.Configuration;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.TestInstance;
import ukrtech.formregister.autotest.Objects.Buttons;
import ukrtech.formregister.autotest.Util.ActionHelper;
import static com.codeborne.selenide.Selenide.*;
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class BaseTest {
    static private Boolean setupComplete = false;

    @BeforeAll
    void setup() {
        if (setupComplete) {
            return;
        }
        initDriver();
        setupComplete = true;
    }

    @AfterEach
    void sweep() {
        open("https://mlmbaza.com/");
        clearBrowserCookies();
        refresh();
        ActionHelper.click(Buttons.btn_reg);
    }
    private void initDriver() {
        Configuration.browserCapabilities.setCapability("acceptInsecureCerts", true);
        Configuration.browser = "chrome";
        Configuration.browserVersion = "103.0";
        Configuration.browserSize = "1920x1080";
        Configuration.timeout = 10000;
        open("https://mlmbaza.com/register/");
    }
}
