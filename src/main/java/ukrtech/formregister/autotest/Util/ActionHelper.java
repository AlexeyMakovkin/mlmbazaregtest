package ukrtech.formregister.autotest.Util;
import com.codeborne.selenide.SelenideElement;
import static com.codeborne.selenide.Selenide.*;

public class ActionHelper {

    public static void click(SelenideElement click) {
        actions().click(click).build().perform();
    }
    public static void sendText(SelenideElement fields, String text) {
        actions().click(fields).sendKeys(text).build().perform();
    }

}
