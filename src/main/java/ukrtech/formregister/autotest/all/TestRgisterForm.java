package ukrtech.formregister.autotest.all;
import com.codeborne.selenide.Condition;
import org.junit.jupiter.api.Test;
import ukrtech.formregister.autotest.BaseTest.BaseTest;
import ukrtech.formregister.autotest.Objects.Buttons;
import ukrtech.formregister.autotest.Objects.Fields;
import ukrtech.formregister.autotest.Util.ActionHelper;
import ukrtech.formregister.autotest.Util.RandomPassword;

public class TestRgisterForm extends BaseTest {

    @Test
    public void createAccountWithFillingInTwoFields(){
        ActionHelper.sendText(Fields.input_login, "dsdgbh");
        ActionHelper.sendText(Fields.input_password, RandomPassword.generateRandomPassword(15));

        ActionHelper.click(Buttons.btn_create_account);

        Fields.error.shouldHave(Condition.text("Необходимо указать корректный Email"));
    }

    @Test
    public void creatingAnAccountWithFillingInThreeFields() {
        ActionHelper.sendText(Fields.input_login, "dsdgbh");
        ActionHelper.sendText(Fields.input_password, RandomPassword.generateRandomPassword(15));
        ActionHelper.sendText(Fields.input_email, "bigban@gmail.com");

        ActionHelper.click(Buttons.btn_create_account);

        Fields.error.shouldHave(Condition.text("Капча не совпадает!"));
    }

    @Test
    public void creatingAnAccountWithMoreThanTheNumberOfCharactersInThePasswordField() {
        ActionHelper.sendText(Fields.input_login, "dsdgbh");
        ActionHelper.sendText(Fields.input_password, RandomPassword.generateRandomPassword(30));
        ActionHelper.sendText(Fields.input_email, "bigban@gmail.com");

        ActionHelper.click(Buttons.btn_create_account);

        Fields.error.shouldHave(Condition.text("Пароль не должен превышать 25 символов"));
    }
}
