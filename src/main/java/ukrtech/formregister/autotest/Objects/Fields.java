package ukrtech.formregister.autotest.Objects;
import com.codeborne.selenide.SelenideElement;
import static com.codeborne.selenide.Selenide.$;

public class Fields {
    public static final SelenideElement input_login = $("input[name='form[login]']");
    public static final SelenideElement input_password = $("input[type='password']");
    public static final SelenideElement input_email = $("input[name='form[email]']");
    public static final SelenideElement input_last_name = $("input[name='form[last_name]']");
    public static final SelenideElement input_first_name = $("input[name='form[first_name]']");
    public static final SelenideElement error = $("div[class='error']");
}
