package ukrtech.formregister.autotest.Objects;
import com.codeborne.selenide.SelenideElement;
import static com.codeborne.selenide.Selenide.$;

public class Buttons {
    public static final SelenideElement btn_reg = $("a[href='/register/']");
    public static final SelenideElement btn_create_account = $("input[name='btn_register']");
}
